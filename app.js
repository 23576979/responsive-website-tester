//get the input field in the home and listen for key presses
document.getElementById('urlInput').addEventListener('keypress', e => {
  //if the key that was pressed was enter then run the code
  if(e.keyCode === 13){
    //remove the default page refresh that occurs on forms when they are submitted 
    e.preventDefault();
    //call this function
    handleSubmit();
  }
})

handleSubmit = () => {
  //get the input and assign it a variable
  let input = document.getElementById('urlInput');
  //get the url that was typed into the url field
  let url = input.value;
  //create a localstorage item that will be accessible from another page and set it to the value of the url
  localStorage.setItem('url', url)
  //load the results page showing the iframe
  window.location.href = 'results.html';
}