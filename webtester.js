//get the localstorage url and set it to a local variable
let url = localStorage.getItem('url');
//get the iframe and set it to a local variable
let iframe1 = document.getElementById('websiteIframe1');
let iframe2 = document.getElementById('websiteIframe2');
//get the parent section containing the iframe and set it to a local variable
let iframeContainer = document.getElementById('display');
//get the url input and set it to a local variable
let input = document.getElementById('currentInput');
//get the device links and set them to local variables
let mobile1 = document.getElementById('mobile1');
let largerMobile1 = document.getElementById('largerMobile1');
let tablet1 = document.getElementById('tablet1');
let laptop1 = document.getElementById('laptop1');
let desktop1 = document.getElementById('desktop1');
let mobile2 = document.getElementById('mobile2');
let largerMobile2 = document.getElementById('largerMobile2');
let tablet2 = document.getElementById('tablet2');
let laptop2 = document.getElementById('laptop2');
let desktop2 = document.getElementById('desktop2');

//get the size indicator p tag and set it to a local variable
let sizeIndicator1 = document.getElementById('sizeIndicator1');
let sizeIndicator2 = document.getElementById('sizeIndicator2');

//set the url from localstorage to the value within the url input on the current page
input.value = url;
//set the current url from localstorage to the iframe src which will load the page within the iframe
iframe1.src = url;
iframe2.src = url;

//click listeners for each of the device links
//each call the same function but pass in specific height and width values that will be utilised within the function
//assign the selected class to the clicked item and remove it for the previously selected item
mobile1.addEventListener('click', () => {
  handleIframeSizing1('375', '667', 'Mobile');
});

largerMobile1.addEventListener('click', () => {
  handleIframeSizing1('411', '823', 'Larger Mobile');
})

tablet1.addEventListener('click', () => {
  handleIframeSizing1('1024', '768', 'Tablet');
})

laptop1.addEventListener('click', () => {
  handleIframeSizing1('1280', '800', 'Laptop');
})

desktop1.addEventListener('click', () => {
  handleIframeSizing1('1680', '1050', 'Desktop');
})


handleIframeSizing1 = (horizontal, vertical, device) => {
  //get the parent iframe container style properties and set the width and height to the width and height parameters passed down through the function
  iframe1.style.width = horizontal + 'px';
  iframe1.style.height = vertical + 'px';
  //set device variable to inner text of notation
  sizeIndicator1.innerHTML = device;
}

//duplicated code that performs a new url query
document.getElementById('currentInput').addEventListener('keypress', e => {
  if(e.keyCode === 13){
    e.preventDefault();
    handleSubmit();
  }
})
handleSubmit = () => {
  let input = document.getElementById('currentInput');
  let url = input.value;
  localStorage.setItem('url', url)
  window.location.href = 'results.html';
}

mobile2.addEventListener('click', () => {
  handleIframeSizing2('375', '667', 'Mobile');
});

largerMobile2.addEventListener('click', () => {
  handleIframeSizing2('411', '823', 'Larger Mobile');
})

tablet2.addEventListener('click', () => {
  handleIframeSizing2('1024', '768', 'Tablet');
})

laptop2.addEventListener('click', () => {
  handleIframeSizing2('1280', '800', 'Laptop');
})

desktop2.addEventListener('click', () => {
  handleIframeSizing2('1680', '1050', 'Desktop');
})
handleIframeSizing2 = (horizontal, vertical, device) => {
  //get the parent iframe container style properties and set the width and height to the width and height parameters passed down through the function
  iframe2.style.width = horizontal + 'px';
  iframe2.style.height = vertical + 'px';
  //set device variable to inner text of notation
  sizeIndicator2.innerHTML = device;
}